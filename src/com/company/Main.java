package com.company;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner x = new Scanner(System.in);
        //В условии дано строго двухзначное число, поэтому проверку или ограничение на кол-во символов не делаю
        System.out.print("Введите число: ");
        int num = x.nextInt();
        int sum = 0;
        while(num != 0){
            sum += num % 10;
            num /= 10;
        }
        System.out.println("Сумма чисел: " + sum);
    }
}
